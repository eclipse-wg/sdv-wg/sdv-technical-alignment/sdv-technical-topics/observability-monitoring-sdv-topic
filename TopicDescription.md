# Unified in-vehicle SW orchestration - “Master Controller”

[Wiki documentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/observability-monitoring-sdv-topic/-/wikis/home) 

# Related SDV projects:
-  [Eclipse eCal](https://projects.eclipse.org/projects/automotive.ecal) 

# Use cases:


# Cross Initiatives Collaboration:
- SOAFEE
    - Tbd – SoC-level, MPAM etc information that might be relevant for big picture
- Tracetest.io
- AUTOSAR (DLT)

# Notes: 

(Reference text from SOAFEE system architecture)
Components shall support observability and analysis by allowing logging at different levels. A standardized and time-synchronized logging mechanism shall be in place. Logging shall support different levels of detail and allow analytics (incl. functional call traceability).
Application Performance Monitoring shall be part of the reference implementation. This shall cover domain-specific use cases as well as health- and system logging.
- Middleware/communication layers likely to require hooks and interfaces to observe infrastructure-level traffic
- A log may be produced by diverse sources including but not limited to: applications, middleware, operating system, hypervisor, secure firmware, devices (event converted into a log).
- Levels/goals/taxonomy of logging/tracing and goals in this area could use more discussion





